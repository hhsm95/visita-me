install:
	pip3 install pipenv
	pipenv --three
	pipenv install

sort:
	@echo "Sorting..."
	isort src/

format:
	@echo "Blacking..."
	black src/

full-format: sort format

requirements:
	@echo "Creating requirements.txt..."
	pipenv run pip freeze > requirements.txt

start:
	@echo "Run Local in Debug..."
	python run.py

pip-lock:
	@echo "Pipenv Lock..."
	pipenv lock
