import os

import sentry_sdk
from dotenv import load_dotenv
from flask import Flask
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_restful import Api
from sentry_sdk.integrations.flask import FlaskIntegration

from .database.db import initialize_db
from .errors import errors

load_dotenv()

SENTRY_API = os.getenv("SENTRY_API")
if SENTRY_API:
    sentry_sdk.init(
        dsn=os.getenv("SENTRY_API"),
        integrations=[FlaskIntegration()],
        traces_sample_rate=1.0,
    )

app = Flask(__name__)
app.config["JWT_SECRET_KEY"] = os.getenv("JWT_SECRET_KEY")
app.config["MONGODB_SETTINGS"] = {"host": os.getenv("MONGO_URL")}

from src.blueprint import initialize_routes

api = Api(app, errors=errors)
bcrypt = Bcrypt(app)
jwt = JWTManager(app)

initialize_db(app)
initialize_routes(api)
