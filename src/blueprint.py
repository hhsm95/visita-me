from .routes.auth import LoginApi, SignupApi
from .routes.reset_password import ForgotPassword, ResetPassword
from .routes.shorts import ShortsApi
from .routes.stats import StatsApi
from .routes.visits import VisitsApi


def initialize_routes(api):
    api.add_resource(SignupApi, "/api/auth/signup")
    api.add_resource(LoginApi, "/api/auth/login")

    api.add_resource(ForgotPassword, "/api/auth/forgot")
    api.add_resource(ResetPassword, "/api/auth/reset")

    api.add_resource(ShortsApi, "/api/shorts")
    api.add_resource(VisitsApi, "/go/<string:short_name>")
    api.add_resource(StatsApi, "/api/stats/<string:short_name>")
