class InternalServerError(Exception):
    pass


class SchemaValidationError(Exception):
    pass


class PageUrlAlreadyExistsError(Exception):
    pass


class EmailAlreadyExistsError(Exception):
    pass


class ShortAlreadyExistsError(Exception):
    pass


class ShortNotExistsError(Exception):
    pass


class InvalidLinkToExistsError(Exception):
    pass


class InvalidShortNameError(Exception):
    pass


class UnauthorizedError(Exception):
    pass


class EmailDoesnotExistsError(Exception):
    pass


class BadTokenError(Exception):
    pass


class ExpiredTokenError(Exception):
    pass


errors = {
    "InternalServerError": {"message": "Something went wrong", "status": 500},
    "SchemaValidationError": {
        "message": "Request is missing required fields",
        "status": 400,
    },
    "PageUrlAlreadyExistsError": {
        "message": "Page with given url already exists",
        "status": 400,
    },
    "EmailAlreadyExistsError": {
        "message": "User with given email address already exists",
        "status": 400,
    },
    "ShortAlreadyExistsError": {
        "message": "Short with given name already exists",
        "status": 400,
    },
    "ShortNotExistsError": {
        "message": "Short with given name not exists",
        "status": 404,
    },
    "InvalidLinkToExistsError": {
        "message": "Not valid URL for the Link To",
        "status": 400,
    },
    "InvalidShortNameError": {
        "message": "Invalid short name error",
        "status": 400,
    },
    "UnauthorizedError": {"message": "Invalid username or password", "status": 401},
    "EmailDoesnotExistsError": {
        "message": "Couldn't find the user with given email address",
        "status": 400,
    },
    "BadTokenError": {"message": "Invalid token", "status": 403},
    "ExpiredTokenError": {"message": "Expired token", "status": 401},
}
