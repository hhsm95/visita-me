from typing import Dict

from flask import request
from user_agents import parse


def get_user_ip() -> str:
    if request.headers.getlist("X-Forwarded-For"):
        ip = request.headers.getlist("X-Forwarded-For")[0]
    else:
        ip = request.remote_addr
    if ", " in ip:
        return ip.split(", ")[0]
    else:
        return ip


def parse_user_agent(user_agent_string: str) -> Dict:
    user_agent = parse(user_agent_string)
    return {
        "browser_family": user_agent.browser.family,
        "browser_version": user_agent.browser.version_string,
        "os_family": user_agent.os.family,
        "os_version": user_agent.os.version_string,
        "device_family": user_agent.device.family,
        "device_brand": user_agent.device.brand,
        "device_model": user_agent.device.model,
    }
