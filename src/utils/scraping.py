import os
from typing import Dict, Optional

import requests

ENV = os.getenv("ENV")


def is_valid_url(url: str) -> bool:
    try:
        requests.head(
            url,
            headers={
                "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) "
                "Chrome/80.0.3987.149 Safari/537.36"
            },
        )
    except requests.ConnectionError:
        return False
    return True


def parse_ip_address(ip_address: str) -> Optional[Dict]:
    url = f"http://ip-api.com/json/{ip_address}"
    ses = requests.session()
    project_url = f"visita-me-{ENV}.herokuapp.com/"
    ses.headers["From"] = project_url
    ses.headers["User-Agent"] = f"visita-me/0.1 (+{project_url})"
    resp = ses.get(url)
    if resp.status_code != 200:
        return None

    data = resp.json()
    if data.pop("status") != "success":
        return None

    return {
        "continent": data.get("continent", ""),
        "country": data.get("country", ""),
        "region": data.get("regionName", ""),
        "city": data.get("city", ""),
        "district": data.get("district", ""),
        "zip_code": data.get("zip", ""),
        "lat": data.get("lat"),
        "lon": data.get("lon"),
        "isp": data.get("isp", ""),
        "org": data.get("org", ""),
        "asname": data.get("asname", ""),
        "mobile": data.get("mobile"),
        "proxy": data.get("proxy"),
    }
