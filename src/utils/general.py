import re


def remove_space(val, space=" ", default=""):
    val = val or default
    return space.join(val.split())


def clean_str(val, default=""):
    return remove_space(val, default=default)


def clean_int(val, default=0):
    if val is None:
        return default
    val = remove_space(val)
    val = re.sub("[^0-9]", "", val)
    if len(val) == 0:
        return default
    return int(val)


def clean_float(val, default=0.0):
    if val is None:
        return default
    val = remove_space(val)
    val = re.sub("[^0-9\.]", "", val)
    if len(val) == 0:
        return default
    return float(val)
