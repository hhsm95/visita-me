import threading

from ..models.shorts import IpAdressInfo, UserAgents, Visits
from ..utils.scraping import parse_ip_address
from ..utils.server import parse_user_agent


class AsyncRegisterVisit(threading.Thread):
    def __init__(
        self, short_id: int, user_agent_string: str, ip_address: str, referer: str
    ):
        super().__init__()
        self.short_id = short_id
        self.user_agent_string = user_agent_string
        self.ip_address = ip_address
        self.referer = referer

    def run(self):
        user_agent = UserAgents.objects.filter(
            user_agent_string=self.user_agent_string
        ).first()
        if not user_agent:
            user_agent_data = parse_user_agent(self.user_agent_string)
            user_agent_data["user_agent_string"] = self.user_agent_string
            user_agent = UserAgents(**user_agent_data)
            user_agent.save()

        ip_address_info = IpAdressInfo.objects.filter(
            ip_address=self.ip_address
        ).first()
        if not ip_address_info:
            ip_address_data = parse_ip_address(self.ip_address) or {}
            ip_address_data["ip_address"] = self.ip_address
            ip_address_info = IpAdressInfo(**ip_address_data)
            ip_address_info.save()

        visit = Visits(
            short_id=self.short_id,
            ip_address=ip_address_info,
            user_agent=user_agent,
            referer=self.referer,
        )
        visit.save()
