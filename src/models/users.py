from flask_bcrypt import check_password_hash, generate_password_hash

from ..database.db import db


class Users(db.Document):
    email = db.EmailField(required=True, unique=True)
    password = db.StringField(required=True, min_length=6)
    is_admin = db.BooleanField(default=False)

    is_active = db.BooleanField(default=True)

    def hash_password(self):
        self.password = generate_password_hash(self.password).decode("utf8")

    def check_password(self, password):
        return check_password_hash(self.password, password)
