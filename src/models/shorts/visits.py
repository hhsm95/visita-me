from datetime import datetime

from ...database.db import db
from .ip_adress_info import IpAdressInfo
from .shorts import Shorts
from .user_agents import UserAgents


class Visits(db.Document):
    short_id = db.ReferenceField("Shorts", required=True)
    ip_address = db.ReferenceField("IpAdressInfo", required=True)
    user_agent = db.ReferenceField("UserAgents", required=True)
    referer = db.StringField(max_length=1000)

    created_at = db.DateTimeField(required=True, default=datetime.utcnow)
