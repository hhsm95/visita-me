from datetime import datetime

from ...database.db import db


class UserAgents(db.Document):
    user_agent_string = db.StringField(required=True, unique=True, max_length=1000)
    browser_family = db.StringField(max_length=100)
    browser_version = db.StringField(max_length=100)
    os_family = db.StringField(max_length=100)
    os_version = db.StringField(max_length=100)
    device_family = db.StringField(max_length=100)
    device_brand = db.StringField(max_length=100)
    device_model = db.StringField(max_length=100)

    created_at = db.DateTimeField(required=True, default=datetime.utcnow)
