from datetime import datetime

from ...database.db import db


class IpAdressInfo(db.Document):
    ip_address = db.StringField(required=True, unique=True, max_length=100)
    continent = db.StringField(max_length=100)
    country = db.StringField(max_length=100)
    region = db.StringField(max_length=100)
    city = db.StringField(max_length=100)
    district = db.StringField(max_length=100)
    zip_code = db.StringField(max_length=100)
    lat = db.FloatField()
    lon = db.FloatField()
    isp = db.StringField(max_length=100)
    org = db.StringField(max_length=100)
    asname = db.StringField(max_length=100)
    mobile = db.BooleanField()
    proxy = db.BooleanField()

    created_at = db.DateTimeField(required=True, default=datetime.utcnow)
