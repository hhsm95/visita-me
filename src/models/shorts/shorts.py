from datetime import datetime

from ...database.db import db


class Shorts(db.Document):
    name = db.StringField(required=True, min_length=2, max_length=20)
    link_to = db.StringField(required=True, min_length=5, max_length=1000)

    created_at = db.DateTimeField(required=True, default=datetime.utcnow)
    created_by = db.ReferenceField("Users", required=True)
    updated_at = db.DateTimeField()
    updated_by = db.ReferenceField("Users")

    is_active = db.BooleanField(required=True, default=True)
