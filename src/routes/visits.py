from flask import redirect, request
from flask_restful import Resource

from ..command.visits import AsyncRegisterVisit
from ..errors import InvalidShortNameError, ShortNotExistsError
from ..models.shorts import Shorts
from ..utils.server import get_user_ip


class VisitsApi(Resource):
    def get(self, short_name: str):
        n = len(short_name)
        if n < 2 or n > 20:
            raise InvalidShortNameError

        short = Shorts.objects.filter(name=short_name).first()
        if not short:
            raise ShortNotExistsError

        ip_address = get_user_ip()
        user_agent_string = request.headers.get("User-Agent", "")
        referer = request.headers.get("Referer", "")

        job = AsyncRegisterVisit(short.id, user_agent_string, ip_address, referer)
        job.start()

        return redirect(short.link_to)
