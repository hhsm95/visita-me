import datetime

from flask import request
from flask_jwt_extended import create_access_token
from flask_restful import Resource
from mongoengine.errors import FieldDoesNotExist, NotUniqueError

from ..errors import EmailAlreadyExistsError, SchemaValidationError, UnauthorizedError
from ..models.users import Users


class SignupApi(Resource):
    def post(self):
        body = request.get_json()
        try:
            user = Users(**body)
        except FieldDoesNotExist:
            raise SchemaValidationError
        except NotUniqueError:
            raise EmailAlreadyExistsError
        user.hash_password()
        user.save()
        id = user.id
        return {"id": str(id)}, 200


class LoginApi(Resource):
    def post(self):
        body = request.get_json()
        user = Users.objects.filter(email=body.get("email")).first()
        if user is None:
            raise UnauthorizedError

        authorized = user.check_password(body.get("password"))
        if not authorized:
            raise UnauthorizedError

        expires = datetime.timedelta(days=7)
        access_token = create_access_token(identity=str(user.id), expires_delta=expires)
        return {"token": access_token}, 200
