import datetime

from flask import request
from flask_jwt_extended import create_access_token, decode_token
from flask_restful import Resource
from jwt.exceptions import DecodeError, ExpiredSignatureError, InvalidTokenError

from ..errors import (
    BadTokenError,
    EmailDoesnotExistsError,
    ExpiredTokenError,
    InternalServerError,
    SchemaValidationError,
)
from ..models.users import Users


class ForgotPassword(Resource):
    def post(self):
        url = f"{request.host_url}reset/"
        try:
            body = request.get_json()
            email = body.get("email")
            if not email:
                raise SchemaValidationError

            user = Users.objects.get(email=email)
            if not user:
                raise EmailDoesnotExistsError

            expires = datetime.timedelta(hours=24)
            reset_token = create_access_token(str(user.id), expires_delta=expires)

            return {"resetUrl": url + reset_token}
        except SchemaValidationError:
            raise SchemaValidationError
        except EmailDoesnotExistsError:
            raise EmailDoesnotExistsError
        except Exception:
            raise InternalServerError


class ResetPassword(Resource):
    def post(self):
        try:
            body = request.get_json()
            reset_token = body.get("reset_token")
            password = body.get("password")

            if not reset_token or not password:
                raise SchemaValidationError

            user_id = decode_token(reset_token)["identity"]

            user = Users.objects.get(id=user_id)

            user.modify(password=password)
            user.hash_password()
            user.save()

            return {"status": "success"}

        except SchemaValidationError:
            raise SchemaValidationError
        except ExpiredSignatureError:
            raise ExpiredTokenError
        except (DecodeError, InvalidTokenError):
            raise BadTokenError
        except Exception:
            raise InternalServerError
