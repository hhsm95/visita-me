import json
from datetime import datetime

from flask_jwt_extended import jwt_required
from flask_restful import Resource

from src.errors import InvalidShortNameError, ShortNotExistsError

from ..models.shorts import Shorts, Visits


class StatsApi(Resource):
    @jwt_required()
    def get(self, short_name: str):
        n = len(short_name or "")
        if n < 2 or n > 20:
            raise InvalidShortNameError

        short = Shorts.objects.filter(name=short_name).first()
        if not short:
            raise ShortNotExistsError

        visits = Visits.objects(short_id=short.id)
        if not visits:
            return {"link_to": short.link_to, "visits": []}, 200

        results = []
        for visit in visits:
            ip_address = json.loads(visit.ip_address.to_json())
            user_agent = json.loads(visit.user_agent.to_json())

            results.append(
                {
                    "referer": visit.referer,
                    "ip_address_info": ip_address,
                    "user_agent_info": user_agent,
                    "created_at": datetime.timestamp(visit.created_at),
                }
            )

        short_dict = json.loads(short.to_json())
        return {"short": short_dict, "visits": results}, 200
