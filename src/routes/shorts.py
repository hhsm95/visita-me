from flask import Response, request
from flask_jwt_extended import get_jwt_identity, jwt_required
from flask_restful import Resource
from mongoengine.errors import FieldDoesNotExist, NotUniqueError

from ..errors import (
    InvalidLinkToExistsError,
    SchemaValidationError,
    ShortAlreadyExistsError,
)
from ..models.shorts import Shorts
from ..utils.scraping import is_valid_url


class ShortsApi(Resource):
    @jwt_required()
    def get(self):
        user_id = get_jwt_identity()
        data = Shorts.objects(created_by=user_id).to_json()
        return Response(data, mimetype="application/json", status=200)

    @jwt_required()
    def post(self):
        user_id = get_jwt_identity()
        payload = request.get_json()
        payload["created_by"] = user_id

        try:
            short = Shorts(**payload)
        except FieldDoesNotExist:
            raise SchemaValidationError
        except NotUniqueError:
            raise ShortAlreadyExistsError

        if not is_valid_url(payload["link_to"]):
            raise InvalidLinkToExistsError

        short.save()
        return {"id": str(short.id)}, 200
